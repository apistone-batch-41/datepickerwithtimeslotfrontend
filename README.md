# datepickerwithtimeslotfrontend 


# Project Description

A Basic Calendar Slot Booking Application which allows people to select their available slots on a day. It also includes month to month and year to year navigation and also has a feature of a Today button that brings focus to the current date. It also includes a Cancel Button which returns focus to the current month.

## Built with

- React JS
- Axios
- Javascript
- Jest
- React Testing Library

## External Tools Used
- Postman

## Usage

![Front-End](imgs/FrontEnd.gif)



## Getting started

To build this project, use the following commands.

```
cd existing_repo
git clone https://gitlab.com/apistone-batch-41/datepickerwithtimeslotfrontend.git
```
After this, use npm install and npm start.
```
npm install
```
```
npm start
```

## Testing

This project has been tested with Jest.


```
npm test

```

## Continous Integration

This project uses a locally hosted runner to test the application. The Pipelines runs automatically on code push to main.

Go to CI/CD -> Pipelines to get logs.

## Authors and acknowledgment

This Project was created by Apistone Team 5.

1. Mousam Baruah
2. Siddharth Ravichandran
3. Triparna Chakraborty
4. Sumeer Garg
5. Abhishek Tiwari





