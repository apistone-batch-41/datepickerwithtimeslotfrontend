import { render, screen } from "@testing-library/react";
import App from "./App";
import DateTimePicker from "./components/DateTimePicker";

test("renders datetimepicker correctly", () => {
  render(<App />);
  const element = screen.getByTestId("date-time-picker-testid");
  expect(element).toBeInTheDocument();
});
