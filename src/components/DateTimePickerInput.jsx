import React from "react";
import "../css/DateTimePickerInput.min.css";
const DateTimePickerInput = (props) => {
  const { inputRef, onClick } = props;
  return (
    <section className="input-container">
      <form className="row">
        <div className="input-group date " onClick={onClick}>
          <input
            type="text"
            placeholder="Select a Date"
            disabled={true}
            className="form-control"
            id="date"
            ref={inputRef}
          />
          <span className="input-group-append">
            <span className="input-group-text bg-light d-block">
              <img src="../cal.ico" id="cal-icon" alt="pick-date" />{" "}
            </span>
          </span>
        </div>
      </form>
    </section>
  );
};

export default DateTimePickerInput;
