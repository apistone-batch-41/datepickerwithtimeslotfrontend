import React from "react";
import "../css/DatePickerFooter.min.css";
const DatePickerFooter = (props) => {
  return (
    <div className="dp-footer " data-testid="date-picker-footer-testid">
      <button
        type="reset"
        className="btn btn-primary dp-footer-btn me-4"
        onClick={props.handleCancel}
      >
        Cancel
      </button>

      <button
        type="submit"
        className="btn btn-primary dp-footer-btn ms-5"
        onClick={() => {
          props.setDateToInput(props.selectedDay);
        }}
      >
        Select
      </button>
    </div>
  );
};

export default DatePickerFooter;
