import React from "react";
import "../css/DatePickerHeader.min.css";
import Years from "./Years";
const DatePickerHeader = (props) => {
  const setYearDropDown = (inputYear) => {
    let year =
      ("" + inputYear).trim() !== "" ? parseInt(inputYear) : props.year;
    let month = props.month;

    props.setYear(year);
    props.setMonthDetails(props.getMonthDetails(year, month));
  };
  const handleYear = (offset) => {
    let year =
      ("" + props.year).length < 4
        ? new Date().getFullYear() + offset
        : props.year + offset;
    let month = props.month;
    props.setYear(year);
    props.setMonthDetails(props.getMonthDetails(year, month));
  };
  const handleMonth = (offset) => {
    let year = props.year;
    let month = props.month + offset;
    if (month === -1) {
      month = 11;
      year--;
    } else if (month === 12) {
      month = 0;
      year++;
    }
    props.setYear(year);
    props.setMonth(month);
    props.setMonthDetails(props.getMonthDetails(year, month));
  };
  return (
    <div className="dp-header" data-testid="date-picker-header-testid">
      <div
        className="dph-arrow-button"
        role="navigation"
        onClick={() => {
          handleYear(-1);
        }}
      >
        <span className="dphb-left-arrows"> </span>
      </div>
      <div
        className="dph-arrow-button"
        role="navigation"
        onClick={() => {
          handleMonth(-1);
        }}
      >
        <span className="dphb-left-arrow"></span>
      </div>
      <div className="dph-year-month" data-testid="dph-year-month-testid">
        <div className="dph-year mb-0" data-testid="dph-year-testid">
          <Years
            year={props.year}
            setYear={setYearDropDown}
            inputRef={props.inputRef}
          />
        </div>
        <div className="dph-month" data-testid="dph-month-testid">
          {props.getMonthStr(props.month)}{" "}
        </div>
      </div>
      <div
        className="dph-arrow-button"
        role="navigation"
        onClick={() => {
          handleMonth(1);
        }}
      >
        <span className="dphb-right-arrow"></span>
      </div>
      <div
        className="dph-arrow-button"
        role="navigation"
        onClick={() => {
          handleYear(1);
        }}
      >
        <span className="dphb-right-arrows"> </span>
      </div>
      <button id="today" onClick={props.setTodaysDate}>
        Today
      </button>
    </div>
  );
};

export default DatePickerHeader;