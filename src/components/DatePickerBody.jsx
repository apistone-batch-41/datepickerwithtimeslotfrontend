import React, { useEffect, useRef } from "react";
import { ToastContainer } from "react-toastify";
import "../css/DatePickerBody.min.css";
let oneDay = 60 * 60 * 24 * 1000;
const DatePickerBody = (props) => {
  let unavailDates = props.unavailableDates;

  const checkUnavailDates = (day) => {
    let month = props.month + 1;
    month = ("" + month).length === 1 ? "0" + month : month;
    let date = day.date;
    date = ("" + date).length === 1 ? "0" + date : date;
    let udate = "" + props.year + "-" + month + "-" + date;

    return unavailDates.includes(udate);
  };
  const datePickerBodyRef = useRef();
  useEffect(() => {
    datePickerBodyRef.current.focus();
  }, []);
  const handleKeyPress = (e) => {
    e.preventDefault();
    let setNewDate = new Date(
      props.getDateStringFromTimestamp(props.selectedDay)
    );
    if (e.keyCode === 37) {
      setNewDate.setDate(setNewDate.getDate() - 1);
      let isUnavailDate = unavailDates.includes(
        props.getDateStringFromTimestamp(getTimestamp(setNewDate))
      );

      setNewDate.setDate(
        isUnavailDate ? setNewDate.getDate() - 1 : setNewDate.getDate()
      );
    } else if (e.keyCode === 38) {
      setNewDate.setDate(setNewDate.getDate() - 7);
      let isUnavailDate = unavailDates.includes(
        props.getDateStringFromTimestamp(getTimestamp(setNewDate))
      );

      setNewDate.setDate(
        isUnavailDate ? setNewDate.getDate() - 7 : setNewDate.getDate()
      );
    } else if (e.keyCode === 39) {
      setNewDate.setDate(setNewDate.getDate() + 1);
      let isUnavailDate = unavailDates.includes(
        props.getDateStringFromTimestamp(getTimestamp(setNewDate))
      );

      setNewDate.setDate(
        isUnavailDate ? setNewDate.getDate() + 1 : setNewDate.getDate()
      );
    } else if (e.keyCode === 40) {
      setNewDate.setDate(setNewDate.getDate() + 7);
      let isUnavailDate = unavailDates.includes(
        props.getDateStringFromTimestamp(getTimestamp(setNewDate))
      );

      setNewDate.setDate(
        isUnavailDate ? setNewDate.getDate() + 7 : setNewDate.getDate()
      );
    }

    props.setSelectedDay(getTimestamp(setNewDate));

    props.setYear(setNewDate.getFullYear());
    props.setMonth(setNewDate.getMonth());
    props.setMonthDetails(
      props.getMonthDetails(setNewDate.getFullYear(), setNewDate.getMonth())
    );
    props.handleTimePicker(true);
  };

  const getTimestamp = (date) => {
    return date - (date % oneDay) + date.getTimezoneOffset() * 1000 * 60;
  };
  let days = props.monthDetails.map((day, index) => {
    return (
      <div
        className={
          "dpb-dates-container" +
          (day.month !== 0
            ? " disabled"
            : checkUnavailDates(day)
            ? " highlight-red"
            : isSelectedDay(day, props.selectedDay)
            ? " highlight-blue"
            : isCurrentDay(day, props.todayTimestamp)
            ? " "
            : "")
        }
        key={index}
      >
        <div className="dpb-dates-container-day">
          <span onClick={async () => await props.onDateClick(day)}>
            {" "}
            {day.date}{" "}
          </span>
        </div>
      </div>
    );
  });

  return (
    <div className="dp-body" data-testid="date-picker-body-testid">
      <ToastContainer />
      <div className="dpb-top-row" data-testid="dpb-top-row-testid">
        {["SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"].map((d, i) => (
          <div key={i} className="dpb-top-row-day">
            {d}
          </div>
        ))}
      </div>
      <div
        className="dpb-dates"
        onKeyDown={handleKeyPress}
        tabIndex="0"
        data-testid="dpb-dates-testid"
        ref={datePickerBodyRef}
      >
        {" "}
        {days}{" "}
      </div>
    </div>
  );
};

const isCurrentDay = (day, todayTimestamp) => {
  return day.timestamp === todayTimestamp;
};
const isSelectedDay = (day, selectedDay) => {
  return day.timestamp === selectedDay;
};

export default DatePickerBody;
