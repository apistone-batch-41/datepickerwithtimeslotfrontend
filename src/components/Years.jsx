import React, { useEffect, useState } from "react";
import { flushSync } from "react-dom";
import "../css/Years.min.css";
import { yearsArray } from "../Sources/arrays";
const Years = (props) => {
  const [filteredYears, setFilteredYears] = useState(yearsArray);
  const [selectedYear, setSelectedYear] = useState(props.year);
  const handleChange = (e) => {
    let search = e.target.value;
    let filteredYears = [];
    if ("" + search !== "") {
      filteredYears = yearsArray.filter((year) => {
        return ("" + year).startsWith(search);
      });

      if (filteredYears.length > 0) setFilteredYears(filteredYears);
      else {
        setFilteredYears(yearsArray);
      }
    } else {
      setFilteredYears(yearsArray);
    }
    console.log("e", e.target.value);
    setSelectedYear(e.target.value);
    props.setYear(e.target.value);
  };
  const handleBlur = (e) => {
    let filteredYears = [];
    let year = new Date().getFullYear();
    filteredYears = yearsArray.filter((year) => {
      return "" + year === e.target.value;
    });
    if (filteredYears.length < 1) {
      console.log("f", filteredYears);
      e.preventDefault();

      if (props.inputRef.current.value.trim() !== "") {
        year = props.inputRef.current.value.split(" ")[0].split("-")[0];
      }

      setSelectedYear(year);
      flushSync(() => props.setYear(year));

      e.target.focus();
    }
  };
  const validateNumber = (event) => {
    const keyCode = event.keyCode;
    console.log("Hello");
    const excludedKeys = [8, 37, 39, 46];

    if (
      !(
        (keyCode >= 48 && keyCode <= 57) ||
        (keyCode >= 96 && keyCode <= 105) ||
        excludedKeys.includes(keyCode) ||
        event.target.value.length > 4
      )
    ) {
      event.preventDefault();
    }
  };
  useEffect(() => {
    setSelectedYear(props.year);
  }, [props.year]);
  return (
    <span>
      <div className="dropdown pick-year">
        <input
          className="btn dropdown-toggle  pick-year-select"
          type="text"
          data-testid="year-input"
          id="dropdownMenuYears"
          data-bs-toggle="dropdown"
          value={selectedYear}
          maxLength={4}
          onChange={handleChange}
          onBlur={handleBlur}
          onKeyDown={validateNumber}
          autoComplete="off"
          onClick={(e) => {
            e.target.focus();
            setFilteredYears(yearsArray);
          }}
        />

        <ul
          className="dropdown-menu float-start"
          id="years"
          data-testid="year-dropdown"
        >
          {filteredYears.map((year, index) => {
            return (
              <React.Fragment key={index}>
                <li className="">
                  <button
                    className="dropdown-item"
                    value={year}
                    onClick={(e) => {
                      setSelectedYear(e.target.value);
                      props.setYear(e.target.value);
                    }}
                  >
                    {year}
                  </button>
                </li>
              </React.Fragment>
            );
          })}
        </ul>
      </div>
    </span>
  );
};

export default Years;