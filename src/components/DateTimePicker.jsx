import React, { useEffect, useRef, useState } from "react";
import DatePicker from "./DatePicker";
import DateTimePickerInput from "./DateTimePickerInput";
import "../css/DateTimePicker.min.css";
import TimePicker from "./TimePicker";

let dateInputRef = React.createRef();
let datePickerRef = React.createRef();
let dateTimePickerRef = React.createRef();
const DateTimePicker = () => {
  const [datePickerOpen, setDatePickerOpen] = useState(false);
  const [timePickerOpen, setTimePickerOpen] = useState("false");
  const [timePickerClass, setTimePickerClass] = useState("d-none");
  let visibility = usePrevious(datePickerOpen);
  const [timePickerRef, setTimePickerRef] = useState("");
  const handleClose = () => {
    setDatePickerOpen(false);
    setTimePickerOpen(false);
    setTimePickerClass("d-none");
  };

  const handleOpen = async () => {
    
    setDatePickerOpen(visibility ? false : true);
    if (datePickerOpen) handleClose();
    else {
      datePickerRef.current.updateDateFromInput();
      if (timePickerRef === "") setTimePickerClass("d-none");
      else setTimePickerOpen(true);
      setDatePickerOpen(true);
    }
  };
  const closeTimePicker = () => {
    setTimePickerOpen(false);
  };
  const handleTimePicker = (display) => {
    setDatePickerOpen(true);
    if (!display) {
      setTimePickerRef("");
    }
    setTimePickerOpen(display);
    setTimePickerClass("");
  };
  const handleOutsideClick = (e) => {
    if (
      dateTimePickerRef.current &&
      !dateTimePickerRef.current.contains(e.target)
    ) {
      console.log("Vis in Child Comp");
      handleClose();
    }
  };
  useEffect(() => {
    document.addEventListener(
      "click",
      (event) => handleOutsideClick(event),
      true
    );
    return () => {
      document.removeEventListener("click", handleOutsideClick, true);
    };
  });

  function usePrevious(value) {
    const ref = useRef();
    useEffect(() => {
      ref.current = value;
    }, [value]); // Only re-run if value changes
    // Return previous value (happens before update in useEffect above)
    return ref.current;
  }

  return (
    <div
      ref={dateTimePickerRef}
      className=" date-time-picker d-flex flex-column align-items-center "
      data-testid="date-time-picker-testid"
    >
      <div className="row">
        <DateTimePickerInput onClick={handleOpen} inputRef={dateInputRef} />
      </div>
      <div className="d-flex m-0">
        <DatePicker
          visibility={datePickerOpen}
          ref={datePickerRef}
          handleClose={handleClose}
          inputRef={dateInputRef}
          handleTimePicker={handleTimePicker}
          timePickerRef={timePickerRef}
          setTimePickerRef={setTimePickerRef}
        />
        <TimePicker
          className=""
          timePickerClass={timePickerClass}
          visibility={timePickerOpen}
          timePickerRef={timePickerRef}
          setTimePickerRef={setTimePickerRef}
          closeTimePicker={closeTimePicker}
        />
      </div>
    </div>
  );
};
export default DateTimePicker;