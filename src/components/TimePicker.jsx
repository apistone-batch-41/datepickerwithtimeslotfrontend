import React from "react";
import "../css/TimePicker.min.css";
import { timePickerArray } from "../Sources/arrays";

const TimePicker = (props) => {
  var setClass =
    props.timePickerClass === "d-none"
      ? "d-none"
      : props.visibility
      ? "slide-out"
      : "slide-in d-none";

  console.log("timeslotclass", setClass);
  const handleTimeChange = async (changeEvent) => {
    console.log("radio click", changeEvent.target.value);
    await props.setTimePickerRef(changeEvent.target.value);
  };

  return (
    <div>
      {" "}
      {props.visibility ? (
        <div className={`tp-container ${setClass}`}>
          <span className="tp-close-icon" onClick={props.closeTimePicker}>
            <i className="fa-solid fa-xmark"></i>
          </span>
          <h5 className="mb-3 mt-1">Select Time</h5>
          <div className="d-flex flex-column justify-content-center align-items-center m-3">
            {timePickerArray.map((time, index) => {
              return (
                <React.Fragment key={index}>
                  <label>
                    <input
                      type="radio"
                      id={index}
                      value={time}
                      className="text-align-start mb-4 timeslot"
                      checked={props.timePickerRef.trim() === time.trim()}
                      onChange={handleTimeChange}
                    />

                    {time}
                  </label>
                </React.Fragment>
              );
            })}
          </div>
        </div>
      ) : (
        ""
      )}
    </div>
  );
};

export default TimePicker;
