import React, {
  forwardRef,
  useEffect,
  useImperativeHandle,
  useState,
} from "react";
import DatePickerHeader from "./DatePickerHeader";
import DatePickerFooter from "./DatePickerFooter";
import DatePickerBody from "./DatePickerBody";
import { toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";
import { daysMap, monthMap } from "../Sources/arrays";
import axios from "axios";
import "../css/DatePicker.min.css";

let oneDay = 60 * 60 * 24 * 1000;
let date = new Date();
let currentYear = date.getFullYear();
let currentMonth = date.getMonth();
let todayTimestamp =
  Date.now() -
  (Date.now() % oneDay) +
  new Date().getTimezoneOffset() * 1000 * 60;

const DatePicker = forwardRef((props, ref) => {
  const [year, setYear] = useState(currentYear);
  const [month, setMonth] = useState(currentMonth);
  const [selectedDay, setSelectedDay] = useState(todayTimestamp);
  const [monthDetails, setMonthDetails] = useState(
    getMonthDetails(currentYear, currentMonth)
  );
  const [unavailableDates, setUnavailableDates] = useState([]);
  useEffect(() => {
    const fetchUnavailableDates = async () => {
      try {
        const response = await axios.get("http://host.docker.internal:8080/invalid");

        let udates = response.data.response.map((obj) => obj.date);
        console.log("unavaildates", udates);
        setUnavailableDates(udates);
      } catch (Exception) {
        toast("Try after sometime...");
      }
    };
    fetchUnavailableDates();
  }, []);
  const setDate = (dateData) => {
    let selectedDay = new Date(
      dateData.year,
      dateData.month - 1,
      dateData.date
    ).getTime();
    setSelectedDay(selectedDay);
    if (props.onChange) {
      props.onChange(selectedDay);
    }
  };

  useImperativeHandle(ref, () => ({
    updateDateFromInput() {
      if (props.inputRef.current.value.trim() !== "") {
        let dateTimeValue = props.inputRef.current.value.split(" ");
        let dateValue = dateTimeValue[0];
        let timeValue = dateTimeValue[1];
        let dateData = getDateFromDateString(dateValue);

        setDate(dateData);

        setYear(dateData.year);
        setMonth(dateData.month - 1);
        setMonthDetails(getMonthDetails(dateData.year, dateData.month - 1));

        if (timeValue !== "") {
          props.setTimePickerRef(timeValue);
        }
      } else {
        setYear(currentYear);
        setMonth(currentMonth);
        setMonthDetails(getMonthDetails(currentYear, currentMonth));
        setSelectedDay(todayTimestamp);
      }
    },
  }));
  const handleCancel = (e) => {
    e.preventDefault();

    props.inputRef.current.value = "";
    let dateData = getDateFromDateString(
      getDateStringFromTimestamp(todayTimestamp)
    );

    setDate(dateData);

    setYear(dateData.year);
    setMonth(dateData.month - 1);
    setMonthDetails(getMonthDetails(dateData.year, dateData.month - 1));
    setSelectedDay(todayTimestamp);

    props.handleTimePicker(false);
  };
  const setTodaysDate = async (e) => {
    e.preventDefault();
    props.inputRef.current.value = "";
    let dateData = getDateFromDateString(
      getDateStringFromTimestamp(todayTimestamp)
    );
    if (
      !unavailableDates.includes(getDateStringFromTimestamp(todayTimestamp))
    ) {
      setDate(dateData);

      setYear(dateData.year);
      setMonth(dateData.month - 1);
      setMonthDetails(getMonthDetails(dateData.year, dateData.month - 1));
      setSelectedDay(todayTimestamp);
      props.setTimePickerRef("");
      props.handleTimePicker(true);
    } else toast("This date is unavailable");
  };

  const getDateFromDateString = (dateValue) => {
    let dateData = dateValue.split("-").map((d) => parseInt(d, 10));
    if (dateData.length < 3) return null;
    let year = dateData[0];
    let month = dateData[1];
    let date = dateData[2];
    return {
      year,
      month,
      date,
    };
  };
  const getMonthStr = (month) =>
    monthMap[Math.max(Math.min(11, month), 0)] || "Month";
  const getDateStringFromTimestamp = (timestamp) => {
    let dateObject = new Date(timestamp);
    let month = dateObject.getMonth() + 1;
    let date = dateObject.getDate();
    return (
      dateObject.getFullYear() +
      "-" +
      (month < 10 ? "0" + month : month) +
      "-" +
      (date < 10 ? "0" + date : date)
    );
  };
  const postDate = async (date, time) => {
    console.log("date and time", date, time);
    try {
      const response = await axios.post(
        "http://host.docker.internal:8080/datetime",
        JSON.stringify({ date: date, timeSlot: time }),
        {
          headers: {
            "Content-Type": "application/json",
          },
        }
      );
      if (response.data.status !== "CREATED") {
        toast(response.data.message);
      } else {
        props.inputRef.current.value = date + " " + time;

        props.handleClose();
      }
    } catch (Exception) {
      toast("Try after sometime...");
    }
  };

  const setDateToInput = () => {
    let dateString = getDateStringFromTimestamp(selectedDay);
    console.log("time", props.timePickerRef);
    postDate(dateString, props.timePickerRef);
  };
  const onDateClick = (day) => {
    let cmonth = month + 1;
    cmonth = ("" + cmonth).length === 1 ? "0" + cmonth : cmonth;
    let cdate = day.date;
    cdate = ("" + cdate).length === 1 ? "0" + cdate : cdate;
    let udate = "" + year + "-" + cmonth + "-" + cdate;
    if (unavailableDates.includes(udate)) {
      toast("This date is unavailable");
    } else {
      setSelectedDay(day.timestamp);
      props.handleTimePicker(true);
    }
  };
  return (
    <div>
      {props.visibility ? (
        <div className="date-picker">
          <DatePickerHeader
            year={year}
            month={month}
            setYear={setYear}
            setMonth={setMonth}
            setMonthDetails={setMonthDetails}
            getMonthDetails={getMonthDetails}
            getMonthStr={getMonthStr}
            inputRef={props.inputRef}
            setTodaysDate={setTodaysDate}
          />
          <DatePickerBody
            monthDetails={monthDetails}
            selectedDay={selectedDay}
            setSelectedDay={setSelectedDay}
            todayTimestamp={todayTimestamp}
            onDateClick={onDateClick}
            unavailableDates={unavailableDates}
            setUnavailableDates={setUnavailableDates}
            year={year}
            month={month}
            handleTimePicker={props.handleTimePicker}
            setMonth={setMonth}
            setYear={setYear}
            getDateStringFromTimestamp={getDateStringFromTimestamp}
            setMonthDetails={setMonthDetails}
            getMonthDetails={getMonthDetails}
          />
          <DatePickerFooter
            handleCancel={handleCancel}
            setDateToInput={setDateToInput}
          />
        </div>
      ) : (
        ""
      )}
    </div>
  );
});
const getNumberOfDays = (year, month) => {
  return 35 - new Date(year, month, 35).getDate();
};
const getDayDetails = (args) => {
  let date = args.index - args.firstDay;
  let day = args.index % 7;
  let prevMonth = args.month - 1;
  let prevYear = args.year;
  if (prevMonth < 0) {
    prevMonth = 11;
    prevYear--;
  }

  let prevMonthNumberOfDays = getNumberOfDays(prevYear, prevMonth);
  let _date =
    (date < 0 ? prevMonthNumberOfDays + date : date % args.numberOfDays) + 1;

  let month = date < 0 ? -1 : date >= args.numberOfDays ? 1 : 0;
  let timestamp = new Date(args.year, args.month, _date).getTime();

  return {
    date: _date,
    day,
    month,

    timestamp,
    dayString: daysMap[day],
  };
};

const getMonthDetails = (year, month) => {
  let firstDay = new Date(year, month).getDay();
  console.log("firstDay", firstDay);
  let numberOfDays = getNumberOfDays(year, month);
  let monthArray = [];
  let rows = 6;
  let currentDay = null;
  let index = 0;
  let cols = 7;
  for (let row = 0; row < rows; row++) {
    for (let col = 0; col < cols; col++) {
      currentDay = getDayDetails({
        index,
        numberOfDays,
        firstDay,
        year,
        month,
      });
      monthArray.push(currentDay);
      index++;
    }
  }
  return monthArray;
};
export default DatePicker;
