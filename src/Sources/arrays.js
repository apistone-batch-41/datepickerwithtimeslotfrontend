import {
  END_YEAR,
  START_YEAR,
  TIME_SLOT_1,
  TIME_SLOT_2,
  TIME_SLOT_3,
  TIME_SLOT_4,
  TIME_SLOT_5,
  TIME_SLOT_6,
  TIME_SLOT_7,
  TIME_SLOT_8,
} from "./constants";

const daysMap = [
  "Sunday",
  "Monday",
  "Tuesday",
  "Wednesday",
  "Thursday",
  "Friday",
  "Saturday",
];
const monthMap = [
  "January",
  "February",
  "March",
  "April",
  "May",
  "June",
  "July",
  "August",
  "September",
  "October",
  "November",
  "December",
];
const timePickerArray = [
  TIME_SLOT_1,
  TIME_SLOT_2,
  TIME_SLOT_3,
  TIME_SLOT_4,
  TIME_SLOT_5,
  TIME_SLOT_6,
  TIME_SLOT_7,
  TIME_SLOT_8,
];
const yearsArray = [];
for (let i = START_YEAR; i < END_YEAR; i++) {
  yearsArray.push(i);
}
export { daysMap, monthMap, timePickerArray, yearsArray };
