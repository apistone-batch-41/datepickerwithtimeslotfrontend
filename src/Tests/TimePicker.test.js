import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import React from "react";
import TimePicker from "../components/TimePicker";

afterEach(cleanup);
describe("TimePicker", () => {
  test("renders timepicker heading correctly", () => {
    render(
      <TimePicker visibility={true} timePickerClass={""} timePickerRef={""} />
    );
    const element = screen.getByRole("heading");
    expect(element).toBeInTheDocument();
  });
  test("renders radio buttons correctly", () => {
    render(
      <TimePicker visibility={true} timePickerClass={""} timePickerRef={""} />
    );
    const element = screen.getAllByRole("radio");
    expect(element).toHaveLength(8);
  });
});
