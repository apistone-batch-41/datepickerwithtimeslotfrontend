import { render, screen, fireEvent, cleanup } from "@testing-library/react";
import React from "react";
import Years from "../components/Years";
afterEach(cleanup);
const setYear = (val) => {};
describe("YearDropdown with input", () => {
  test("renders  input box correctly", () => {
    render(<Years setYear={setYear} />);
    const element = screen.getByTestId("year-input");
    expect(element).toBeInTheDocument();
  });

  test("renders  dropdown correctly", () => {
    render(<Years setYear={setYear} />);
    const element = screen.getByTestId("year-dropdown");
    expect(element).toBeInTheDocument();
  });

  test("year selected successfully", () => {
    const { getByTestId, getAllByRole } = render(<Years setYear={setYear} />);
    fireEvent.click(getAllByRole("button")[1]);
    let options = getByTestId("year-input");
    expect(options.value).toBe("2024");
  });
});
