import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import React, { useState } from "react";
import DatePicker from "../components/DatePicker";
import DatePickerBody from "../components/DatePickerBody";

afterEach(cleanup);
describe("DatePickerBody1", () => {
  test("renders datepicker top row correctly", () => {
    render(<DatePickerBody monthDetails={[]} />);
    const element = screen.getByTestId("dpb-top-row-testid");
    expect(element).toBeInTheDocument();
  });
});

describe("DatePickerBody2", () => {
  test("renders datepicker dates correctly", () => {
    render(<DatePickerBody monthDetails={[]} />);
    const element = screen.getByTestId("dpb-dates-testid");
    expect(element).toBeInTheDocument();
  });
});
