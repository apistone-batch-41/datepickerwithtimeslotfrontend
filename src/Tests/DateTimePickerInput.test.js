import { cleanup, render, screen } from "@testing-library/react";
import DateTimePickerInput from "../components/DateTimePickerInput";

afterEach(cleanup);
test("Rendered Successfully", () => {
  render(<DateTimePickerInput />);
  const element = screen.getByPlaceholderText("Select a Date");
  expect(element).toBeInTheDocument();
});

test("Rendered Successfully", () => {
  render(<DateTimePickerInput />);
  const element = screen.getByRole("img");
  expect(element).toBeInTheDocument();
});
