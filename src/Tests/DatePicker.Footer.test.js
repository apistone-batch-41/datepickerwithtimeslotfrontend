import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import React, { useState } from "react";
import DatePickerFooter from "../components/DatePickerFooter";

afterEach(cleanup);
describe("DatePickerFooter", () => {
  test("renders  cancel correctly", () => {
    render(<DatePickerFooter />);
    const element = screen.getByText("Cancel");
    expect(element).toBeInTheDocument();
  });

  test("renders  select correctly", () => {
    render(<DatePickerFooter />);
    const element = screen.getByText("Select");
    expect(element).toBeInTheDocument();
  });
});
