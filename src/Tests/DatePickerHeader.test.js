import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import React, { useState } from "react";
import DatePickerHeader from "../components/DatePickerHeader";

afterEach(cleanup);
const getMonthStr = (month) => {
  return month;
};
describe("DatePickerHeader1", () => {
  test("renders datepicker arrows correctly", () => {
    render(<DatePickerHeader getMonthStr={getMonthStr} />);
    const element = screen.getAllByRole("navigation");
    expect(element).toHaveLength(4);
  });
});

describe("DatePickerHeader2", () => {
  test("renders datepicker year and month correctly", () => {
    render(<DatePickerHeader getMonthStr={getMonthStr} />);
    const element = screen.getByTestId("dph-year-month-testid");
    expect(element).toBeInTheDocument();
  });
});
describe("DatePickerHeader3", () => {
  test("renders datepicker year correctly", () => {
    render(<DatePickerHeader getMonthStr={getMonthStr} />);
    const element = screen.getByTestId("dph-year-testid");
    expect(element).toBeInTheDocument();
  });
});
describe("DatePickerHeader4", () => {
  test("renders datepicker month correctly", () => {
    render(<DatePickerHeader getMonthStr={getMonthStr} />);
    const element = screen.getByTestId("dph-month-testid");
    expect(element).toBeInTheDocument();
  });
});
describe("DatePickerHeader5", () => {
  test("renders  today correctly", () => {
    render(<DatePickerHeader getMonthStr={getMonthStr} />);
    const element = screen.getByText("Today");
    expect(element).toBeInTheDocument();
  });
});
