import { render, screen, cleanup, fireEvent } from "@testing-library/react";
import React, { useState } from "react";
import DatePicker from "../components/DatePicker";

afterEach(cleanup);
describe("DatePicker1", () => {
  test("renders datepicker header correctly", () => {
    render(<DatePicker visibility={true} />);
    const element = screen.getByTestId("date-picker-header-testid");
    expect(element).toBeInTheDocument();
  });
});

describe("DatePicker2", () => {
  test("renders datepicker footer correctly", () => {
    render(<DatePicker visibility={true} />);
    const element = screen.getByTestId("date-picker-footer-testid");
    expect(element).toBeInTheDocument();
  });
});
describe("DatePicker3", () => {
  test("renders datepicker body correctly", () => {
    render(<DatePicker visibility={true} />);
    const element = screen.getByTestId("date-picker-body-testid");
    expect(element).toBeInTheDocument();
  });
});
