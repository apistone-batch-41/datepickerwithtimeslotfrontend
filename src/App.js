import "./App.min.css";
import DateTimePicker from "./components/DateTimePicker";

function App() {
  return (
    <div className="App">
      <h3 className="mb-4"> DatePicker with Timeslot</h3>
      <DateTimePicker />
    </div>
  );
}

export default App;
